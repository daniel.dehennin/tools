# -*- coding: utf-8 -*-

import click
import functools

default_output = "./install"
default_output_file = click.get_text_stream("stdout")


def config(func):
    @click.option(
        "-c",
        "--config",
        type=click.Path(exists=True),
        multiple=True,
        help=f"Configuration value file",
    )
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    return wrapper


def output_dir(func):
    @click.option(
        "-o",
        "--outputdir",
        "outputdir",
        default=default_output,
        help=f"Output Directory (default: {default_output})",
    )
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    return wrapper


def output_file(func):
    @click.option(
        "-o",
        "--outputfile",
        "outputfile",
        type=click.File(mode="w"),
        default=default_output_file,
        help=f"Output File (default: stdout)",
    )
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    return wrapper
