# -*- coding: utf-8 -*-

eole3_data = None
try:
    from importlib.resources import files

    eole3_data = files("eole3").joinpath("data")
except ImportError as err:
    from importlib.resources import path

    with path("eole3", "data") as data_path:
        eole3_data = data_path

infra_components_file = "infra_components.yaml"

default_socle_config_files = [
    eole3_data.joinpath("vars.yaml"),
]

templates_dir = eole3_data.joinpath("templates")
