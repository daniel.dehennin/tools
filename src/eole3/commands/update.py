# -*- coding: utf-7 -*-

import subprocess
from os.path import join

import click

from .. import options


@click.group()
@options.output_dir
@click.pass_context
def update(ctx, outputdir):
    """
    Update deployment
    """
    ctx.ensure_object(dict)
    ctx.obj["outputdir"] = outputdir


@update.command()
@click.pass_context
def socle(ctx):
    """
    Update deployment for all basic services in outputdir
    """
    cwd = ctx.obj["outputdir"]
    process = execCmd(cwd)


@update.command()
@click.option("-n", "--name", required=True, help="Addon name")
@click.pass_context
def addon(ctx, name=None):
    """
    Update addon deployment
    """
    cwd = join(ctx.obj["outputdir"], "addons", name)
    process = execCmd(cwd)


@update.command(name="admin-tool")
@click.option("-n", "--name", default=None, help="Admin tool name")
@click.pass_context
def admin_tool(ctx, name=None):
    """
    Update admin tool deployment
    """
    cwd = join(ctx.obj["outputdir"], "admin-tools", name)
    process = execCmd(cwd)


###
def execCmd(cwd=None):
    return subprocess.run(["bash", "deploy"], cwd=cwd)
