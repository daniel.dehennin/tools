# -*- coding: utf-8 -*-

from os import makedirs
from os.path import basename
from os.path import dirname
from os.path import isfile
from os.path import isdir
from os.path import join
from os.path import relpath

from jinja2 import Environment
from jinja2 import FileSystemLoader

import click
import io

from .. import options
from .. import data
from .. import utils


@click.group()
@options.config
@click.pass_context
def config(ctx, config):
    """
    Show configuration merged from all the configuration files

    \b
    - the default ones `data.default_config_files`
    - the files specified by `--config` options
    """
    ctx.ensure_object(dict)
    ctx.obj["custom_socle_config_files"].extend(list(config))


@config.command()
@options.config
@options.output_file
@click.pass_context
def socle(ctx, config, outputfile):
    """
    Show merge configuration for socle
    """
    ctx.obj["outputfile"] = outputfile
    show_config(ctx)


@config.command()
@click.option("-n", "--name", required=True, help="Addon name")
@options.config
@options.output_file
@click.pass_context
def addon(ctx, config, outputfile, name=None):
    """
    Show merged configuration for addon
    """
    ctx.obj["custom_extra_config_files"] = list(config)
    ctx.obj["outputfile"] = outputfile
    show_extra_config(ctx, "addons", name)


@config.command(name="admin-tool")
@click.option("-n", "--name", default=None, help="Admin tool name")
@options.config
@options.output_file
@click.pass_context
def admin_tool(ctx, config, outputfile, name=None):
    """
    Show merged configuration for an admin tool
    """
    ctx.obj["custom_extra_config_files"] = list(config)
    ctx.obj["outputfile"] = outputfile
    show_extra_config(ctx, "admin-tools", name)


def show_extra_config(ctx, app_type, name):
    """
    Show merged configuration for any extra tool (`addon` or `admin-tool`)
    """
    extra_dir = data.eole3_data.joinpath(app_type, name)

    ctx.obj["default_extra_config_files"] = [extra_dir.joinpath(f"{name}-vars.yaml")]
    show_config(ctx, app_type)


def show_config(ctx=None, app_type="socle"):
    """
    Print merged configuration
    """
    if app_type == "socle":
        config_files = ctx.obj["default_socle_config_files"]
        config_files.extend(ctx.obj["custom_socle_config_files"])
    if app_type != "socle":
        config_files = ctx.obj["default_extra_config_files"]
        config_files.extend(ctx.obj["custom_extra_config_files"])

    utils.yaml.dump(
        utils.merge_config(config_files=config_files), ctx.obj["outputfile"]
    )
