# -*- coding: utf-8 -*-

from os import makedirs
from os.path import basename
from os.path import dirname
from os.path import isfile
from os.path import isdir
from os.path import join
from os.path import relpath
from copy import deepcopy

from jinja2 import Environment
from jinja2 import FileSystemLoader

import click

from .. import options
from .. import data
from .. import utils


@click.group()
@options.config
@options.output_dir
@click.pass_context
def build(ctx, config, outputdir):
    """
    Generate templates
    """
    ctx.ensure_object(dict)
    ctx.obj["custom_socle_config_files"].extend(list(config))
    ctx.obj["outputdir"] = outputdir
    initialiseEnv(ctx)


@build.command()
@click.pass_context
def socle(ctx):
    """
    Generate all templates for all basic services in outputdir
    """
    genInfra(ctx)
    initialiseEnv(ctx)
    ctx.obj["output_path"] = ctx.obj["outputdir"]
    click.echo("Generate templates for laboite")
    genTemplates(ctx)


@build.command()
@click.option("-n", "--name", required=True, help="Infra name")
@click.pass_context
def infra(ctx, name=None):
    """
    Generate infra component
    """
    genExtra(ctx, "infra", name)


@build.command()
@click.option("-n", "--name", required=True, help="Addon name")
@options.config
@click.pass_context
def addon(ctx, config, name=None):
    """
    Generate addon
    """
    ctx.obj["custom_extra_config_files"] = list(config)
    genExtra(ctx, "addons", name)


@build.command(name="admin-tool")
@click.option("-n", "--name", default=None, help="Admin tool name")
@options.config
@click.pass_context
def admin_tool(ctx, config, name=None):
    """
    Generate admin tool
    """
    ctx.obj["custom_extra_config_files"] = list(config)
    genExtra(ctx, "admin-tools", name)


###
def genInfra(ctx, app_type=None, name=None):
    """
    Generate infra components configs
    """
    if app_type != None:
        app_dir = data.eole3_data.joinpath(app_type, name)
        components_file = app_dir.joinpath(data.infra_components_file)
    else:
        components_file = data.eole3_data.joinpath(data.infra_components_file)
    with open(components_file, "r") as infra:
        components = utils.yaml.load(infra, Loader=utils.yaml.FullLoader)["components"]
    ctx.obj["readme"] = False
    ctx.obj["output_path"] = ctx.obj["outputdir"]
    for component in components:
        genExtra(ctx, "infra", component)


###
def genExtra(ctx, app_type, name):
    """
    Generate extra config
    """
    click.echo("Generate templates for " + name)
    extra_dir = data.eole3_data.joinpath(app_type, name)
    extra_templates_dir = extra_dir.joinpath("templates")

    extra_loader = FileSystemLoader([extra_templates_dir, data.templates_dir])
    jinja_env = ctx.obj["jinja_env"].overlay(loader=extra_loader)
    ctx.obj["jinja_env"] = jinja_env
    if isfile(extra_dir.joinpath(f"{name}-vars.yaml")):
        ctx.obj["default_extra_config_files"] = [
            extra_dir.joinpath(f"{name}-vars.yaml")
        ]
    else:
        ctx.obj["default_extra_config_files"] = []

    ctx.obj["output_path"] = join(ctx.obj["outputdir"], app_type, name)

    genTemplates(ctx, root_dir=extra_dir)


###
def genTemplates(ctx=None, root_dir=None):
    """
    Generate all templates
    """
    root_dir = root_dir or ""
    templates_root_dir = data.eole3_data.joinpath(root_dir, "templates")
    template_dirs = [
        "templates",
        "templates/resources",
        "templates/scripts",
        "templates/docs",
    ]
    # Add config files in order
    config_files = deepcopy(ctx.obj["default_socle_config_files"])
    config_files.extend(ctx.obj["custom_socle_config_files"])
    config_files.extend(ctx.obj["default_extra_config_files"])
    config_files.extend(ctx.obj["custom_extra_config_files"])

    config = utils.merge_config(config_files=config_files)

    for template_dir in template_dirs:
        template_path = data.eole3_data.joinpath(root_dir, template_dir)
        for template in template_path.glob("*"):
            if not isfile(template):
                continue

            template = relpath(template, templates_root_dir)
            genTemplate(ctx, template=template, config=config)


def genTemplate(ctx=None, template=None, config=None):
    """
    Generate and save a single template
    """
    stdout = None
    if ctx.obj["readme"]:
        stdout = template == "docs/README"

    env = ctx.obj["jinja_env"]
    tmpl = env.get_template(template)
    output = tmpl.render(config=config, output_dir=ctx.obj["output_path"])
    if stdout:
        click.echo(output)

    writeFile(
        filepath=ctx.obj["output_path"], filename=basename(template), filecontent=output
    )


def writeFile(filepath=None, filename=None, filecontent=None):
    """
    Write a file in a directory, create the directory if it does not exists
    """
    if not isdir(filepath):
        makedirs(filepath)
    with open(join(filepath, filename), "w") as output:
        output.write(filecontent)


def initialiseEnv(ctx):
    default_loader = FileSystemLoader(data.templates_dir)
    default_env = Environment(
        loader=default_loader, extensions=["jinja2.ext.do", utils.RaiseExtension]
    )
    default_env.globals["get_value"] = utils.get_value
    default_env.filters["from_json"] = utils.from_json
    ctx.obj["jinja_env"] = default_env
    ctx.obj["default_extra_config_files"] = []
    ctx.obj["custom_extra_config_files"] = []
    ctx.obj["readme"] = True
