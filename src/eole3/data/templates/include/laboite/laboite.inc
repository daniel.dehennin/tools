ingress_redirect:
  {%- if config['default']['ingress']['controller'] == "nginx"  %}
  className: nginx
  {%- if config['cert-manager']['enabled'] %}
  annotations:
    nginx.ingress.kubernetes.io/configuration-snippet: return 302 https://{{ config['laboite']['hostname'] }}.{{ config['default']['domain'] }}/;
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
    nginx.ingress.kubernetes.io/whitelist-source-range: {{ module['whitelistIps'] }}
  {% else %}     
  annotations:
    nginx.ingress.kubernetes.io/configuration-snippet: return 302 https://{{ config['laboite']['hostname'] }}.{{ config['default']['domain'] }}/;
    nginx.ingress.kubernetes.io/whitelist-source-range: {{ module['whitelistIps'] }}
  {%- endif %}
  {%- endif %}
  enabled: true
  # Defines hosts to add to ingress routes
  hosts:
  - host: {{ config['default']['domain'] }}
    paths:
    - path: /
      pathType: ImplementationSpecific
  # Use TLS for these FQDN
  tls:
  - hosts:
    - {{ config['default']['domain'] }}
    {%- if config['cert-manager']['enabled'] %}
    secretName: tls-{{ config['laboite']['hostname'] }}-redirect
    {%- endif %}

