# Parameters to connect to mongodb database
mongoName: {{ config['mongodb']['mongoName'] }}.{{ get_value(config, 'mongodb', 'namespace') }}
mongoPort: "{{ config['mongodb']['mongoPort'] }}"
mongoDatabase: {{ config['mongodb']['mongoDatabase'] }}
mongoRsname: {{ config['mongodb']['mongoRsname'] }}
mongoUsername: {{ config['mongodb']['mongoUsername'] }}
mongoPassword: {{ config['mongodb']['mongoPassword'] }}

# Keycloak parameters
keycloak_url: {{ config['keycloak']['hostname'] }}.{{ config['default']['domain'] }}
keycloak_realm: {{ config['keycloak']['realm'] }} 

