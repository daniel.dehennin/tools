{%- from "include/postgresql/restore-script.j2" import postgresql_restore_script -%}
{%- from "include/s3/restore-script.j2" import s3_restore_script -%}

#!/bin/bash

{% include 'include/utils/log.sh.j2' %}

BACKUP_NAMESPACE="backup"

log "Restore MongoDB"
kubectl apply -n ${BACKUP_NAMESPACE} -f mongodb-credentials.yaml

kubectl apply -n ${BACKUP_NAMESPACE} -f mongodb-restore-job.yaml
kubectl wait -n  ${BACKUP_NAMESPACE} --for=condition=complete --timeout=600s job/mongodb-restore
kubectl -n ${BACKUP_NAMESPACE} logs job/mongodb-restore
kubectl delete -n ${BACKUP_NAMESPACE} -f mongodb-restore-job.yaml

cd infra/keycloak/
bash restore.sh
cd -

{{ s3_restore_script(config, 'laboite') }}
