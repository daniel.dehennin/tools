{%- from "include/postgresql/backup-script.j2" import postgresql_backup_script -%}
{%- from "include/s3/backup-script.j2" import s3_backup_script -%}

#!/bin/bash

{% include 'include/utils/log.sh.j2' %}

BACKUP_NAMESPACE="backup"
[ ! -e ${BACKUP_NAMESPACE} ] && mkdir -p ${BACKUP_NAMESPACE}

kubectl create namespace ${BACKUP_NAMESPACE} --dry-run=client -o yaml | kubectl apply -f -

log "Backup MongoDB"
kubectl apply -n ${BACKUP_NAMESPACE} -f mongodb-backup-pvc.yaml

kubectl apply -n ${BACKUP_NAMESPACE} -f mongodb-credentials.yaml

kubectl apply -n ${BACKUP_NAMESPACE} -f mongodb-backup-cronjob.yaml
kubectl create -n ${BACKUP_NAMESPACE} job --from=cronjob/backup-mongodb-schedule mongodb-backup
kubectl wait -n ${BACKUP_NAMESPACE} --for=condition=complete --timeout=600s job/mongodb-backup
kubectl -n ${BACKUP_NAMESPACE} logs job/mongodb-backup
#kubectl -n ${BACKUP_NAMESPACE} cp mount-backups-volume:/data/laboite ${BACKUP_NAMESPACE}/laboite
{%- if not config['mongodb']['enableBackup'] %}
kubectl delete -n ${BACKUP_NAMESPACE} -f mongodb-backup-cronjob.yaml
{%- endif %}

cd infra/keycloak/
bash backup.sh
cd -

{{ s3_backup_script(config, 'laboite') }}
