{%- if get_value(config, 'shlink', 'database:provider') == 'mariadb' %}
{%- from "include/mariadb/backup-script.j2" import mariadb_backup_script as backup_script -%}
{%- else %}
{%- from "include/postgresql/backup-script.j2" import postgresql_backup_script as backup_script -%}
{%- endif %}

#!/bin/bash

{{ backup_script(config, 'shlink') }}
