#!/bin/bash

cat <<EOF >> /bitnami/matomo/config/config.ini.php
[LoginOIDC]
authenticationName = "{{ config['keycloak']['realm'] }}"
authorizeUrl = "https://{{ config['keycloak']['hostname'] }}.{{ config['default']['domain'] }}/auth/realms/{{ config['keycloak']['realm'] }}/protocol/openid-connect/auth"
tokenUrl = "https://{{ config['keycloak']['hostname'] }}.{{ config['default']['domain'] }}/auth/realms/{{ config['keycloak']['realm'] }}/protocol/openid-connect/token"
userinfoUrl = "https://{{ config['keycloak']['hostname'] }}.{{ config['default']['domain'] }}/auth/realms/{{ config['keycloak']['realm'] }}/protocol/openid-connect/userinfo"
endSessionUrl = "https://{{ config['keycloak']['hostname'] }}.{{ config['default']['domain'] }}/auth/realms/{{ config['keycloak']['realm'] }}/protocol/openid-connect/logout?redirect_uri=https://{{ config['matomo']['hostname'] }}.{{ config['default']['domain'] }}"
userinfoId = "sub"
clientId = "{{ config['keycloak']['clientName'] }}"
clientSecret = "{{ config['keycloak']['clientSecret'] }}"
scope = "openid email"
allowSignup = true
EOF
php /bitnami/matomo/console plugin:activate LoginOIDC

