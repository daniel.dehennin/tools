{%- from "include/postgresql/restore-script.j2" import postgresql_restore_script -%}

#!/bin/bash

BACKUP_NAMESPACE="backup"

{{ postgresql_restore_script(config, 'keycloak', 'statefulsets') }}
