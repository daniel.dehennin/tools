{%- from "include/postgresql/backup-script.j2" import postgresql_backup_script -%}

#!/bin/bash

BACKUP_NAMESPACE="backup"
[ ! -e ${BACKUP_NAMESPACE} ] && mkdir -p ${BACKUP_NAMESPACE}

kubectl create namespace ${BACKUP_NAMESPACE} --dry-run=client -o yaml | kubectl apply -f -

{{ postgresql_backup_script(config, 'keycloak') }}
