{%- from "include/s3/backup-script.j2" import s3_backup_script -%}

#!/bin/bash

{{ s3_backup_script(config, 'loki') }}
