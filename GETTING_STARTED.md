# Utilisation de l'outil eole3

## Démarrage rapide

### Installation de l'outil eole3

Afin de ne pas polluer votre système, nous conseillons l’utilisation un environnement virtuel ([`venv`](https://docs.python.org/3/library/venv.html)).

En fonction de votre distribution, vous devez installer le paquet nécessaire à la gestion des `venv`
```
apt update
apt install python3-venv
```

Il faut ensuite créer l’environnement dédié à l’outil
```
python3 -m venv venv
```

Une fois installé, il faut activer l’environnement afin que l’installation et l’exécution des commandes se déroulent à l’intérieur
```
source venv/bin/activate
```

Ensuite installez le paquet python `eole3`
```
pip install eole3 --index-url https://gitlab.mim-libre.fr/api/v4/projects/494/packages/pypi/simple
```

Activer la complétion pour la commande `eole3`
- Ajouter cette ligne au fichier `~/.bashrc` (pour le shell `bash`)
```
eval "$(_EOLE3_COMPLETE=bash_source eole3)"

```

### Liste des commandes eole3

```
eole3 --help
Usage: eole3 [OPTIONS] COMMAND [ARGS]...

Options:
  -c, --config PATH  Configuration value file
  --version          Show the version and exit.
  --help             Show this message and exit.

Commands:
  config  Show configuration merged from all the configuration files
  mcm     Generate minimized configuration file with only non-default values
  build   Generate templates
  deploy  Deploy generated templates
  update  Update deployment

```

### Installation du socle de base

1. Voir la configuration par défaut `eole3 config socle`
2. Créer un fichier `socle.yaml` et l'adapter à votre environnement. Il faut au minimum adapter la valeur de la variable `general.domain` à votre nom de domaine
3. Lancer la génération des fichiers avec la commande `eole3 --config socle.yaml build socle`
4. Copier la clef privée (`tls.key`) et le certificat (`tls.crt`) wildcard de votre nom de domaine dans le répertoire `./install/infra/ingress-nginx/`
5. Déployer la configuration avec la commande suivante `eole3 deploy socle`

### Mise à jour du socle de base

Pour mettre à jour le socle, il vous faut :
1. Éditer le fichier `socle.yaml` et mettre à jour les options
2. Lancer la génération des fichiers avec la commande `eole3 --config socle.yaml build socle`
3. Mettre à jour la configuration avec la commande suivante `eole3 update socle`

### Installation d'un service additionnel (exemple pour Nextcloud)

1. Voir la configuration par défault `eole3 config addon -n nextcloud`
2. Lancer la génération des fichiers avec la commande `eole3 --config socle.yaml build addon -n nextcloud`
3. Déployer la configuration avec la commande suivante `eole3 deploy addon -n nextcloud`

### Installation d'un outil d'administration (exemple pour SuperCRUD)

1. Voir la configuration par défault `eole3 config admin-tool -n supercrud`
2. Lancer la génération des fichiers avec la commande `eole3 --config socle.yaml build admin-tool -n supercrud`
3. Déployer la configuration avec la commande suivante `eole3 deploy admin-tool -n supercrud`

## Configuration avancée

### Gestion des fichiers de configuration

La création de vos fichiers de configuration se fait en plusieurs étapes.

#### Le socle de base

1. Extraction des valeurs par défaut
```
eole3 config socle -o socle.yaml
```
2. Adaptation des valeurs à votre environnement en modifiant le fichier `socle.yaml`
3. Génération du fichier de configuration minimal pour votre environnement
```
eole3 --config socle.yaml mcm socle -o minimal-socle.yaml
```

#### Les service additionnels

1. Extraction des valeurs par défaut
```
eole3 config addon -n <my-addon> -o <my-addon>.yaml
```
2. Adaptation des valeurs à votre environnement en modifiant le fichier `<my-addon>.yaml`
3. Génération du fichier de configuration minimal pour votre environnement
```
eole3 mcm addon -n <my-addon> --config <my-addon>.yaml -o minimal-<my-addon>.yaml
```

#### Les service d'administration

1. Extraction des valeurs par défaut
```
eole3 config admin-tool -n <my-admin-tool> -o <my-admin-tool>.yaml
```
2. Adaptation des valeurs à votre environnement en modifiant le fichier `<my-admin-tool>.yaml`
3. Génération du fichier de configuration minimal pour votre environnement
```
eole3 mcm admin-tool -n <my-admin-tool> --config <my-admin-tool>.yaml -o minimal-<my-admin-tool>.yaml
```

### Génération des fichiers nécessaires au déploiement

La génération des fichiers de déploiement se fait à l'aide de la commande `build`

#### Le socle de base

`eole3 --config minimal-socle.yaml --output /path/to/my-install build socle`

#### Les service additionnels

L'option `--config` de l'addon doit être après la commande `build addon`, sinon les valeurs par défaut de l'addon seront appliquées.

`eole3 --config minimal-socle.yaml build addon -n <my-addon> --config <my-addon>.yaml`

#### Les outils d'administration

L'option `--config` de l'admin-tool doit être après la commande `build admin-tool`, sinon les valeurs par défaut de l'admin-tool seront appliquées.

`eole3 --config minimal-socle.yaml build admin-tool -n <my-admin-tool> --config <my-admin-tool>.yaml`

### Construction et déploiment avancés

Par défaut les commandes `build` et `deploy` prennent en charge la totalité des composants du socle. Il est néanmoins possible de les traiter séparemment.

#### Construction des composants d'infrastructure

Les composants d'infrastructure listés dans le tableau du [fichier README](README.md) peuvent être construits individuellement:

`eole3 --config minimal-socle.yaml build infra -n <my-infra-component>`

#### Déploiment des composants d'infrastructure

Les composants d'infrastructure peuvent être déployés individuellement:

`eole3 deploy infra -n <my-infra-component>`

## Gestion de la configuration

La section `default` contient les paramètres par défaut nécessaires au déploiement des composants.
Ces paramètres peuvent être surchargés suivant les besoins des différents composants. \
Il est par exemple possible de définir certains paramètres de ressources spécifiques à `laboite` :
```
laboite:
  resources:
    requests:
      memory: 1200Mi
    limits:
      memory: 1200Mi
```

Tous les services de `laboite` seront déployés dans le `namespace` choisi pour `laboite`.
Pour les autres composants, il est préfarable de définir un `namespace` propre à chacun. \
Dans ce cas, il suffit de déclarer la variable `namespace` dans les sections correspondantes. \
Exemple pour `mongodb` :
```
mongodb:
  namespace: mongodb
```

Ainsi, il est possible d'utiliser un serveur `postgresql` comme composant d'infrastructure du `socle` pour un addon et de paramétrer un serveur de base de données externe pour `keycloak`.
Il en est de même avec le serveur `s3`.
